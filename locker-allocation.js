/*
  Problem: assigning locker to class in a way that minimize distance.

  Input:
  * An array of number in_stack indicating how many lockers are available in a stack.
  * An array of number in_classes indicating how many lockers each class need.
  * An 2d array of number in_dist[i, j] indicating how far away class i is from stack j.

  Input constrain:
  * ( Sum i=0 to last_class of in_classes[i] ) <= ( Sum j=0 to last_stack of in_stack[j] )

  Output:
  A 2d array of number: [i, j] indicates the number of locker class i get from
  stack j, such that cost(output, in_dist) is minimum.

  Output constrain:
  * ( Sum i=0 to last_class of output[i, j] ) <= in_stack[j]
  * ( Sum j=0 to last_stack of output[i, j] ) == in_classes[i]
*/

function validateInput (stack, classes, dist) {
  if (!Array.isArray(stack)) throw new TypeError('stack should be an Array.')
  if (!Array.isArray(classes)) throw new TypeError('classes should be an Array.')
  if (!Array.isArray(dist)) throw new TypeError('dist should be an Array.')
  if (dist.length !== classes.length) throw new RangeError('dist should be an Array with length ' + classes.length)
  for (let c of dist) {
    if (!Array.isArray(c)) throw new TypeError('dist should be a 2d Array.')
    if (c.length !== stack.length) throw new RangeError(`dist should be a 2d Array with dimansion [${classes.length}, ${stack.length}]`)
    for (let d of c) if (!Number.isFinite(d)) throw new TypeError('dist[][] should be a float.')
  }
  for (let s of stack) if (!Number.isSafeInteger(s) || s < 0) throw new TypeError('stack should be an Array of non-negative integers.')
  for (let s of classes) if (!Number.isSafeInteger(s) || s < 0) throw new TypeError('classes should be an Array of non-negative integers.')

  let diff = stack.reduce((a, b) => a + b) - classes.reduce((a, b) => a + b)
  if (diff < 0) throw new Error(`Not enough locker for all classes: need ${-diff} more.`)

  return true
}

/*
  Optimal:
*/

function permuteIterate (permutation, callback) {
  let length = permutation.length
  let c = new Array(length).fill(0)
  let i = 1

  callback(permutation)

  while (i < length) {
    if (c[i] < i) {
      let k = 0
      if (i % 2 === 1) k = c[i]
      let swap = permutation[i]
      permutation[i] = permutation[k]
      permutation[k] = swap
      c[i] ++
      i = 1
      callback(permutation)
    } else {
      c[i] = 0
      i ++
    }
  }
}

function algorithm_permutations (stack, classes, dist) {
  function runRound (permu) {
    let output = []
    for (let i = 0; i < classes.length; i ++) {
      output[i] = new Array(stack.length).fill(0)
    }

    let stackRemaining = permu.map(x => Object.assign({}, x))
    let firstRemainingStack = 0
    let classRemaining = classes.slice()
    let firstClassRemaining = 0
    while (true) {
      if (classRemaining[firstClassRemaining] === 0) {
        firstClassRemaining ++
      }
      if (firstClassRemaining >= classes.length) break
      if (stackRemaining[firstRemainingStack].s === 0) {
        firstRemainingStack ++
      }
      if (firstRemainingStack >= stackRemaining.length) {
        throw new Error('Input constrain failure: not enough lockers.')
      }
      let needAlloc = Math.min(classRemaining[firstClassRemaining], stackRemaining[firstRemainingStack].s)
      stackRemaining[firstRemainingStack].s -= needAlloc
      classRemaining[firstClassRemaining] -= needAlloc
      output[firstClassRemaining][stackRemaining[firstRemainingStack].i] += needAlloc
    }

    return output
  }

  let minCost = Infinity
  let minCostSolution = null

  stack = stack.map((s, i) => ({s, i}))

  let totalPerm = 1
  for (let i = 1; i <= stack.length; i ++) {
    totalPerm *= i
  }
  totalPerm --
  let currentPerm = 0
  let progPad = totalPerm.toString().length
  permuteIterate(stack, permu => {
    let output = runRound(permu)
    let curCost = cost(output, dist)
    if (curCost < minCost) {
      minCost = curCost
      minCostSolution = output
    }
    currentPerm ++
  })

  return minCostSolution || []
}

let algorithm = algorithm_permutations

function validateOutput (inStack, inClasses, output) {
  if (!Array.isArray(output)) throw new TypeError('output is not an Array.')
  if (output.length !== inClasses.length) throw new RangeError(`output should be an Array of dimansion [${inClasses.length}, ${inStack.length}].`)
  for (let row of output) {
    if (!Array.isArray(row)) {
      throw new TypeError('output is not an Array of Array.')
    } else {
      for (let col of row) if (!Number.isSafeInteger(col) || col < 0) throw new TypeError('output is not an Array of Array of non-negative integers.')
    }
  }
  for (let row of output) if (row.length !== inStack.length) throw new RangeError(`output should be an Array of dimansion [${inClasses.length}, ${inStack.length}].`)
  for (let j = 0; j < inStack.length; j ++) {
    let sumAllocated = 0
    for (let row of output) {
      sumAllocated += row[j]
    }
    if (sumAllocated > inStack[j]) throw new Error(`Output constrain failure: out of lockers for stack ${j}`)
  }
  for (let i = 0; i < inClasses.length; i ++) {
    let sumGiven = output[i].reduce((a, b) => a + b)
    if (sumGiven !== inClasses[i]) throw new Error(`Output constrain failure: expected ${inClasses[i]} lockers for class ${i}, but only ${sumGiven} given.`)
  }
}

function cost (output, dist) {
  let cost = 0
  for (let i = 0; i < output.length; i ++) {
    for (let j = 0; j < output[i].length; j ++) {
      cost += dist[i][j] * output[i][j]
    }
  }
  return cost
}

module.exports = { validateInput, algorithm, validateOutput, cost }
