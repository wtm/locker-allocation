'use strict'
const React = require('react')
const ReactDOM = require('react-dom')
const Algorithm = require('./locker-allocation.js')
const NcicMap = require('./ncic.js')

require('babel-polyfill')
require('./layout.sass')

function roundFloating (f, accuracy = 10000, floor = false) {
  return (floor ? Math.floor : Math.round)(f * accuracy) / accuracy
}
function client2view (point, view) {
  // Copied from schsrch by maowtm.
  let rect = view.getBoundingClientRect()
  if (rect.left === 0 && rect.top === 0)
    console.warn("client2view won't work if the view isn't affecting layout. (e.g. display: none)")
  var supportPageOffset = window.pageXOffset !== undefined
  var isCSS1Compat = ((document.compatMode || "") === "CSS1Compat")
  var scrollX = supportPageOffset ? window.pageXOffset : isCSS1Compat ? document.documentElement.scrollLeft : document.body.scrollLeft
  var scrollY = supportPageOffset ? window.pageYOffset : isCSS1Compat ? document.documentElement.scrollTop : document.body.scrollTop
  return [point[0] - (rect.left + scrollX), point[1] - (rect.top + scrollY)]
}

class LockerAllocationApp extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      groundWidth: 16,
      groundHeight: 26,
      currentTool: null,
      contentWidth: null,
      contentHeight: null,
      objects: [],
      currentEdit: null,
      selecting: null,
      allocation: null,
      allocError: null,
      undoStack: [],
      redoStack: []
    }
    this.handleDown = this.handleDown.bind(this)
    this.handleMove = this.handleMove.bind(this)
    this.handleUp = this.handleUp.bind(this)
    this.boundDocEvents = false
  }
  getCoordinateConverters () {
    let scale = null
    let offset = null
    let ground2view = point => [0, 0]
    let view2ground = point => [0, 0]
    if (this.state.contentWidth !== null && this.state.contentHeight !== null && this.state.groundWidth > 0 && this.state.groundHeight > 0) {
      let sX = this.state.contentWidth / this.state.groundWidth
      let sY = this.state.contentHeight / this.state.groundHeight
      scale = Math.min(sX, sY) / 1.05
      offset = [0, 0]
      let nW = this.state.groundWidth * scale
      offset[0] = (this.state.contentWidth - nW) / 2
      offset[1] = (this.state.contentHeight - this.state.groundHeight * scale) / 2
      ground2view = point => point.map((grd, p) => grd * scale + offset[p])
      view2ground = point => point.map((vid, p) => roundFloating((vid - offset[p]) / scale, 1, true))
    }
    return {scale, offset, ground2view, view2ground}
  }
  render () {
    let {scale, offset, ground2view} = this.getCoordinateConverters()
    return (
      <div className='lockerapp'>
        <div className='menu'>
          <div className='appname'>
            Locker Allocation App
          </div>
          <div className='sup'>Computer only</div>
          <div className='r'>
            <a className={this.state.undoStack.length ? '' : 'disabled'} onClick={evt => this.undo()}>Undo</a>
            <a className={this.state.redoStack.length ? '' : 'disabled'} onClick={evt => this.redo()}>Redo</a>
            &nbsp;&nbsp;&nbsp;&nbsp;
            Load ncic map:&nbsp;
            <a onClick={evt => this.ncic(2)}>2f</a>
            <a onClick={evt => this.ncic(3)}>3f</a>
            <a onClick={evt => this.ncic(4)}>4f</a>
            <a onClick={evt => this.ncic(5)}>5f</a>
            <a onClick={evt => this.ncic(6)}>6f</a>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <a onClick={evt => this.browseFile()}>Load from file&hellip;</a>
            <a onClick={evt => this.saveFile()}>Save to file&hellip;</a>
            &nbsp;&nbsp;&nbsp;&nbsp;
            Written by <a href='https://maowtm.org'>Mao Wtm</a>
          </div>
        </div>
        <div className='content' ref={f => this.measureTarget = f}
          onMouseDown={this.handleDown} >
          {scale === null ? <span>&nbsp;</span> : null}
          {scale !== null ? (() => {
            let lines = []
            for (let x = 0; x < this.state.groundWidth; x ++) {
              let point = ground2view([x, 0])
              lines.push(
                <div className='line' key={'line' + lines.length} style={{
                  left: point[0] + 'px',
                  top: '0px',
                  width: '1px',
                  bottom: '0px'
                }} />
              )
            }
            for (let y = 0; y < this.state.groundHeight; y ++) {
              let point = ground2view([0, y])
              lines.push(
                <div className='line' key={'line' + lines.length} style={{
                  left: '0px',
                  top: point[1] + 'px',
                  height: '1px',
                  right: '0px'
                }} />
              )
            }
            let objects = this.state.objects.map((obj, i) => {
              let pos = ground2view(obj.pos)
              let size = obj.size.map(x => x * scale)
              let vertical = size[0] < size[1]
              let content = null
              let selected = this.state.selecting === obj
              if (obj.type === 'locker') {
                if (vertical) {
                  content = (
                    <div className='ct vertical'>
                      <div className='l'>L</div>
                      <div className='from'>{obj.from}</div>
                      <div className='to'>{obj.to}</div>
                      {this.state.allocation && this.state.allocation.has(obj)
                        ? (
                            <div className='alloc'>{this.state.allocation.get(obj)}</div>
                          )
                        : (
                            <div className='total'>tot.<br />{obj.to + 1 - obj.from}</div>
                          )}
                    </div>
                  )
                } else {
                  content = (
                    <div className='ct horizontal'>
                      <div className='l'>
                        <div>L</div>
                      </div>
                      <div className='r'>
                        <div className='t'>
                          <div className='from'>{obj.from}</div>
                          <div className='to'>{obj.to}</div>
                        </div>
                        {this.state.allocation && this.state.allocation.has(obj)
                          ? (
                              <div className='alloc'>{this.state.allocation.get(obj)}</div>
                            )
                          : (
                              <div className='total'>tot. {obj.to + 1 - obj.from}</div>
                            )}
                      </div>
                    </div>
                  )
                }
              } else if (obj.type === 'classroom') {
                content = (
                  <div className={'ct ' + (vertical ? 'vertical' : 'horizontal')}>
                    <div className='l'><div>C</div></div>
                    <div className='r'>{obj.class} <span className='numPeople'>({obj.numPeople}){
                      this.state.allocation && this.state.allocation.has(obj)
                        ? (
                            <span className='alloc'>: {this.state.allocation.get(obj)}</span>
                          ) : null
                    }</span></div>
                  </div>
                )
              } else if (obj.type === 'door') {
                content = (
                  <div className={'ct ' + (vertical ? 'vertical' : 'horizontal')}>
                    <div className='l'><div>{obj.name || 'D'}</div></div>
                  </div>
                )
              }
              return (
                <div className={'object ' + obj.type + (selected ? ' sel' : '')} key={'obj' + i} style={{
                  left: pos[0] + 'px',
                  top: pos[1] + 'px',
                  width: size[0] + 'px',
                  height: size[1] + 'px'
                }}>
                {content}
                </div>
              )
            })
            return objects.concat(lines).reverse()
          })() : null}
        </div>
        {this.state.selecting === null && !this.state.allocation && !this.state.allocError
          ? (
              <div className='bottombar'>
                Ground size:&nbsp;
                <input type='number' value={this.state.groundWidth} step='1' min='0'
                  onChange={evt => {this.setState({groundWidth: Math.max(0, parseInt(evt.target.value))})}} />
                &nbsp;x&nbsp;
                <input type='number' value={this.state.groundHeight} step='1' min='0'
                  onChange={evt => {this.setState({groundHeight: Math.max(0, parseInt(evt.target.value))})}} />
                <span className='line'>&nbsp;</span>
                Place:&nbsp;
                <span className={'btn' + (this.state.currentTool === 'locker' ? ' current' : '')} onClick={evt => this.toggleTool('locker')}>Group of locker</span>&nbsp;
                <span className={'btn' + (this.state.currentTool === 'classroom' ? ' current' : '')} onClick={evt => this.toggleTool('classroom')}>Classroom</span>
                <span className={'btn' + (this.state.currentTool === 'door' ? ' current' : '')} onClick={evt => this.toggleTool('door')}>Door mark</span>
                <span className='line'>&nbsp;</span>
                Edit:&nbsp;
                <span className={'btn' + (this.state.currentTool === 'move' ? ' current' : '')} onClick={evt => this.toggleTool('move')}>Move</span>
                <span className={'btn' + (this.state.currentTool === 'resize' ? ' current' : '')} onClick={evt => this.toggleTool('resize')}>Resize</span>
                <span className='line'>&nbsp;</span>
                <span className='btn' onClick={evt => this.startAlgorithm()}>Figure out best allocation{this.state.objects.filter(x => x.type === 'locker').length >= 10 ? ' (will take quite long.)' : null}</span>
              </div>
            ) : null}
        {this.state.selecting !== null && !this.state.allocation && !this.state.allocError
          ? (
              <div className='bottombar'>
                Selected&nbsp;
                {(() => {
                  let selected = this.state.selecting
                  if (selected.type === 'locker') {
                    return (
                      <span>
                        group of locker:&nbsp;
                        locker numbering from&nbsp;
                        <input type='number' value={selected.from} step='1' min='0'
                          onChange={evt => this.changeSelectedPropertyInteger('from', evt.target.value)} />
                        &nbsp;to&nbsp;
                        <input type='number' value={selected.to} step='1' min='0'
                          onChange={evt => this.changeSelectedPropertyInteger('to', evt.target.value)} />
                      </span>
                    )
                  }
                  if (selected.type === 'classroom') {
                    return (
                      <span>
                        classroom&nbsp;
                        <input type='text' value={selected.class}
                          onChange={evt => this.changeSelectedPropertyString('class', evt.target.value)} />
                        &nbsp;with&nbsp;
                        <input type='number' value={selected.numPeople} step='1' min='0'
                          onChange={evt => this.changeSelectedPropertyInteger('numPeople', evt.target.value)} />
                        &nbsp;people.
                      </span>
                    )
                  }
                  if (selected.type === 'door') {
                    return (
                      <input type='text' value={typeof selected.name === 'string' ? selected.name : 'door'} placeholder='D' step='1' min='0'
                        onChange={evt => this.changeSelectedPropertyString('name', evt.target.value)} />
                    )
                  }
                  return null
                })()}
                <span className='line'>&nbsp;</span>
                <span className='btn' onClick={evt => this.deleteSelected()}>Delete</span>
              </div>
            ) : null}
          {this.state.allocation && !this.state.selecting
            ? (
                <div className='bottombar'>
                  <span className='btn' onClick={evt => this.setState({allocation: null, allocError: null})}>Hide allocation</span>
                  <span className='line'>&nbsp;</span>
                  Click on a class/locker group to see detail.
                </div>
              ) : null}
          {this.state.allocation && this.state.selecting
            ? ( this.state.allocation.has(this.state.selecting)
                  ? (
                      <div className='bottombar'>
                        {this.state.selecting.type === 'locker' ? 'Locker assignment: ' : `Class ${this.state.selecting.class} got: `}
                        {this.state.allocation.get(this.state.selecting) || 'nothing.'}
                      </div>
                    )
                  : (
                      <div className='bottombar'>
                        -
                      </div>
                    )
              ) : null}
          {this.state.allocError && !this.state.allocation
            ? (
                <div className='bottombar'>
                  <span className='error'>{this.state.allocError}</span>
                  <span className='line'>&nbsp;</span>
                  <span className='btn' onClick={evt => this.setState({allocation: null, allocError: null})}>Close</span>
                </div>
              ) : null}
      </div>
    )
  }
  componentDidMount () {
    this.measureDim()
    window.getObjectsJSON = () => {
      return JSON.stringify(this.state.objects)
    }
    let save = window.localStorage.getItem('save')
    if (typeof save === 'string') {
      try {
        save = JSON.parse(save)
        if (!save.objects) throw new TypeError()
        let selecting = null
        if (save.selectingIndex !== null) {
          selecting = save.objects[save.selectingIndex]
          if (!selecting) selecting = null
        }
        this.setState({
          objects: save.objects,
          undoStack: [],
          redoStack: [],
          selecting,
          currentTool: save.currentTool || null,
          currentEdit: null,
          allocation: null,
          allocError: null,
          groundWidth: save.width || 16,
          groundHeight: save.height || 26
        })
      } catch (e) {
        console.error(e)
        console.error('Unable to laod from localStorage.')
      }
    }
  }
  componentDidUpdate () {
    this.measureDim()
    let selectingIndex = null
    if (this.state.selecting) {
      selectingIndex = this.state.objects.indexOf(this.state.selecting)
      if (selectingIndex < 0) {
        selectingIndex = null
      }
    }
    let save = JSON.stringify({
      objects: this.state.objects,
      selectingIndex,
      currentTool: this.state.currentTool,
      width: this.state.groundWidth,
      height: this.state.groundHeight
    })
    window.localStorage.setItem('save', save)
  }
  measureDim () {
    if (!this.measureTarget) {
      this.setState({contentWidth: null, contentHeight: null})
      return
    }
    let cs = window.getComputedStyle(this.measureTarget)
    let nState = {contentWidth: parseFloat(cs.width) || 0, contentHeight: parseFloat(cs.height) || 0}
    if (Math.abs(this.state.contentWidth - nState.contentWidth) < 1
      && Math.abs(this.state.contentHeight - nState.contentHeight) < 1) return
    this.setState(nState)
  }

  toggleTool (tool) {
    if (this.state.currentTool === tool) {
      this.setState({currentTool: null})
    } else {
      this.setState({
        currentTool: tool,
        selecting: null
      })
    }
  }

  handleDown (evt) {
    if (!this.measureTarget) return
    try {
      evt.preventDefault()
    } catch (e) {}
    let {scale, view2ground} = this.getCoordinateConverters()
    let relPoint = client2view([evt.clientX, evt.clientY], this.measureTarget)
    if (this.state.currentEdit) {
    } else {
      let pos = view2ground(relPoint)
      if (this.state.currentTool !== null) {
        if (this.state.currentTool === 'locker') {
          if (this.rayObject(pos)) return
          let newObject = {
            type: 'locker',
            from: 0,
            to: 0,
            pos: pos,
            size: [1, 1]
          }
          this.setState({
            objects: this.state.objects.concat([newObject]),
            undoStack: this.state.undoStack.concat([this.state.objects]), redoStack: [],
            currentEdit: {
              type: 'resize',
              target: newObject
            }
          })
        } else if (this.state.currentTool === 'classroom') {
          if (this.rayObject(pos)) return
          let newObject = {
            type: 'classroom',
            class: 'Unnamed',
            numPeople: 1,
            pos: pos,
            size: [1, 1]
          }
          this.setState({
            objects: this.state.objects.concat([newObject]),
            undoStack: this.state.undoStack.concat([this.state.objects]), redoStack: [],
            currentEdit: {
              type: 'resize',
              target: newObject
            }
          })
        } else if (this.state.currentTool === 'door') {
          if (this.rayObject(pos)) return
          let newObject = {
            type: 'door',
            pos: pos,
            size: [1, 1]
          }
          this.setState({
            objects: this.state.objects.concat([newObject]),
            undoStack: this.state.undoStack.concat([this.state.objects]), redoStack: [],
            currentEdit: null
          })
        } else if (this.state.currentTool === 'resize') {
          let obj = this.rayObject(pos)
          if (!obj) return
          this.setState({
            currentEdit: {
              type: 'resize',
              target: obj
            }
          })
        } else if (this.state.currentTool === 'move') {
          let obj = this.rayObject(pos)
          if (!obj) return
          this.setState({
            currentEdit: {
              type: 'move',
              target: obj,
              offset: [0, 1].map(p => obj.pos[p] - pos[p])
            }
          })
        }
      } else {
        let obj = this.rayObject(pos)
        this.setState({
          selecting: obj,
          currentTool: null,
          currentEdit: null
        })
      }
    }
    if (!this.boundDocEvents) {
      document.addEventListener('mousemove', this.handleMove, {passive: false})
      document.addEventListener('mouseup', this.handleUp, {passive: false})
      this.boundDocEvents = true
    }
  }
  handleMove (evt) {
    if (!this.measureTarget) return
    try {
      evt.preventDefault()
    } catch (e) {}
    let {scale, view2ground} = this.getCoordinateConverters()
    let relPoint = client2view([evt.clientX, evt.clientY], this.measureTarget)
    if (this.state.currentEdit) {
      let pos = view2ground(relPoint)
      if (this.state.currentEdit.type === 'resize') {
        let target = this.state.currentEdit.target
        let nSize = [0, 1].map(p => Math.max(1, pos[p] - target.pos[p]))
        if (target.size[0] === nSize[0] && target.size[1] === nSize[1]) return
        let nTarget = Object.assign({}, target, {
          size: nSize
        })
        this.setState({
          currentEdit: Object.assign({}, this.state.currentEdit, {
            target: nTarget
          }),
          objects: this.state.objects.map(t => t === target ? nTarget : t),
          undoStack: this.state.undoStack.concat([this.state.objects]), redoStack: []
        })
      } else if (this.state.currentEdit.type === 'move') {
        let target = this.state.currentEdit.target
        let nPos = [0, 1].map(p => pos[p] + this.state.currentEdit.offset[p])
        if (target.pos[0] === nPos[0] && target.pos[1] === nPos[1]) return
        let nTarget = Object.assign({}, target, {
          pos: nPos
        })
        this.setState({
          currentEdit: Object.assign({}, this.state.currentEdit, {
            target: nTarget
          }),
          objects: this.state.objects.map(t => t === target ? nTarget : t),
          undoStack: this.state.undoStack.concat([this.state.objects]), redoStack: []
        })
      }
    }
  }
  handleUp (evt) {
    try {
      evt.preventDefault()
    } catch (e) {}
    if (this.state.currentEdit) {
      this.setState({
        currentEdit: null
      })
    }
    if (this.boundDocEvents) {
      document.removeEventListener('mousemove', this.handleMove)
      document.removeEventListener('mouseup', this.handleUp)
      this.boundDocEvents = false
    }
  }

  componentWillUnmount () {
    window.getObjectsJSON = null
    if (this.boundDocEvents) {
      document.removeEventListener('mousemove', this.handleMove)
      document.removeEventListener('mouseup', this.handleUp)
      this.boundDocEvents = false
    }
  }

  rayObject (pos) {
    for (let ls of this.state.objects) {
      if (ls.pos[0] <= pos[0] && ls.pos[1] <= pos[1] &&
          ls.pos[0] + ls.size[0] > pos[0] && ls.pos[1] + ls.size[1] > pos[1]) return ls
    }
    return null
  }

  deleteSelected () {
    if (this.state.selecting !== null) {
      this.setState({
        objects: this.state.objects.filter(x => x !== this.state.selecting),
        undoStack: this.state.undoStack.concat([this.state.objects]), redoStack: [],
        selecting: null,
        allocation: null,
        allocError: null
      })
    }
  }
  changeSelectedPropertyInteger (prop, val) {
    let sel = this.state.selecting
    if (sel === null) return
    val = parseInt(val)
    if (Number.isNaN(val) || !Number.isSafeInteger(val)) val = 0
    let nSel = Object.assign({}, sel)
    nSel[prop] = val
    this.setState({
      objects: this.state.objects.map(x => x === sel ? nSel : x),
      undoStack: this.state.undoStack.concat([this.state.objects]), redoStack: [],
      selecting: nSel,
      allocation: null,
      allocError: null
    })
  }
  changeSelectedPropertyString (prop, val) {
    let sel = this.state.selecting
    if (sel === null) return
    val = val.trim()
    let nSel = Object.assign({}, sel)
    nSel[prop] = val
    this.setState({
      objects: this.state.objects.map(x => x === sel ? nSel : x),
      undoStack: this.state.undoStack.concat([this.state.objects]), redoStack: [],
      selecting: nSel,
      allocation: null,
      allocError: null
    })
  }

  startAlgorithm () {
    let classes = this.state.objects.filter(x => x.type === 'classroom')
    let stack = this.state.objects.filter(x => x.type === 'locker')
    if (classes.length === 0 || stack.length === 0) return
    let inClasses = classes.map(x => x.numPeople)
    let inStack = stack.map(x => x.to + 1 - x.from)
    let inDists = classes.map(cl => {
      return stack.map(st => {
        // // Taxicab distance, because that's how people walk in buildings.
        // let [xDist, yDist] = [0, 1].map(p => Math.abs((st.pos[p] * 2 + st.size[p]) / 2 - (cl.pos[p] * 2 + cl.size[p]) / 2))
        // return xDist + yDist
        let stpos = [0, 1].map(p => (st.pos[p] * 2 + st.size[p]) / 2)
        let clpos = [0, 1].map(p => (cl.pos[p] * 2 + cl.size[p]) / 2)
        return Math.pow(stpos[0] - clpos[0], 2) + Math.pow(stpos[1] - clpos[1], 2)
      })
    })
    let {validateInput, algorithm, validateOutput, cost} = Algorithm
    try {
      validateInput(inStack, inClasses, inDists)
      console.log('validateInput ok.')
      let output = algorithm(inStack, inClasses, inDists)
      validateOutput(inStack, inClasses, output)
      console.log('validateOutput ok. cost = ' + cost(output, inDists))
      console.log(output)
      let lockerUsed = new Array(stack.length).fill(0)
      let allocation = new Map()
      for (let i = 0; i < output.length; i ++) {
        let allocStr = []
        for (let j = 0; j < output[i].length; j ++) {
          if (output[i][j] === 0) continue
          let locker = stack[j]
          let startFrom = locker.from + lockerUsed[j]
          allocStr.push(`${startFrom}-${startFrom + output[i][j] - 1}`)
          lockerUsed[j] += output[i][j]
        }
        allocStr = allocStr.join(', ')
        allocation.set(classes[i], allocStr)
      }
      for (let j = 0; j < stack.length; j ++) {
        let allocStr = []
        let sum = 0
        for (let i = 0; i < output.length; i ++) {
          if (output[i][j] === 0) continue
          let className = classes[i].class
          sum += output[i][j]
          allocStr.push(`${className} got ${output[i][j]}`)
        }
        let stackTotal = stack[j].to + 1 - stack[j].from
        if (allocStr.length > 0) {
          allocStr.push(`totaling up to ${sum}/${stackTotal} (${roundFloating(sum / stackTotal * 100, 10)}%, ${stackTotal - sum} left)`)
        } else {
          allocStr = [`(${stackTotal} empty lockers)`]
        }
        allocStr = allocStr.join(', ')
        allocation.set(stack[j], allocStr)
      }
      this.setState({allocation, allocError: null})
    } catch (e) {
      this.setState({allocError: e.message, allocation: null})
      console.error(e)
    }
  }

  browseFile () {
    let inp = document.createElement('input')
    inp.type = 'file'
    inp.click()
    inp.addEventListener('change', evt => {
      let fl = evt.target.files
      if (fl.length === 1) {
        let f= fl[0]
        let fr = new FileReader()
        fr.onload = evt => {
          let res = evt.target.result
          if (!res) throw new Error('!res')
          try {
            let obj = JSON.parse(res)
            this.loadSavedObject(obj)
          } catch (e) {
            console.error(e)
          }
        }
        fr.readAsText(f)
      }
    })
  }

  loadSavedObject (obj) {
    if (obj.version !== 0 || !obj.objects) throw new Error('Format invalid.')
    this.setState({
      objects: obj.objects,
      groundWidth: obj.width,
      groundHeight: obj.height,
      currentEdit: null,
      selecting: null,
      allocation: null,
      allocError: null,
      currentTool: null,
      undoStack: [],
      redoStack: []
    })
  }

  saveFile () {
    let json = JSON.stringify({
      version: 0,
      objects: this.state.objects,
      width: this.state.groundWidth,
      height: this.state.groundHeight
    })
    let blob = new Blob([json], {type: 'application/json'})
    let url = URL.createObjectURL(blob)
    let a = document.createElement('a')
    a.href = url
    a.download = 'locker-map.json'
    a.click()
    URL.revokeObjectURL(url)
  }

  ncic (floor) {
    if (NcicMap[floor]) {
      this.loadSavedObject(NcicMap[floor])
    }
  }

  undo () {
    let us = this.state.undoStack
    if (us.length > 0) {
      if (us[us.length - 1] !== this.state.objects) {
        this.setState({
          objects: us[us.length - 1],
          undoStack: us.slice(0, us.length - 1),
          redoStack: this.state.redoStack.concat([this.state.objects])
        })
      } else if (us.length >= 2) {
        this.setState({
          objects: us[us.length - 2],
          undoStack: us.slice(0, us.length - 2),
          redoStack: this.state.redoStack.concat([this.state.objects])
        })
      }
    }
  }

  redo () {
    let rs = this.state.redoStack
    if (rs.length > 0) {
      this.setState({
        objects: rs[rs.length - 1],
        redoStack: rs.slice(0, rs.length - 1),
        undoStack: this.state.undoStack.concat([this.state.objects])
      })
    }
  }
}

let ui = ReactDOM.render(
  <LockerAllocationApp />,
  document.getElementsByClassName('react-root')[0]
)

window.addEventListener('resize', evt => {
  setTimeout(() => ui.forceUpdate(), 1)
})
